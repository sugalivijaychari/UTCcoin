In the present days, we know every one is rushing towards technology and all are investing time to adopt Blockchain technologies into their businesses. Now-a-days, upcoming startups are moving towards initial coin offering which can be the independent monetary system to control the complete eco-system of their organisation as well some external parts of the organisation.

The main moto of initial coin offering is to create a new crypto currency and distribute their newly created tokens in to the market by enorous promotions and discounts for the future value proportion.

This tokens can have the real monetary value like as same as ethereum and bitcoin have. These tokens can be used by everyone according to the valid places for exchange of these tokens by a respective company or organisation.

The implementation of cryptocurrency is placed in path: https://gitlab.com/apogeetechglobalinc/utccoin/tree/master/CryptoCurrency

We can say the initial coin offering as a systematic procedure to distribute the tokens into public, investers as well as developers.

The reason behind this robust eco system is blockchain particularly ethereum blockchain as every transaction is permanently recorded in ethereum and no one can change it.

Hence, the practical implementation of the whole eco system can be possible with smart contracts written through solidity language.

The path for the smart contracts of "Initial Coin Offering" is : https://gitlab.com/apogeetechglobalinc/utccoin/tree/master/InitialCoinOffering/Contracts

1.Basic Architecture

    Firstly, the basic architecture of tokens is placed in path: https://gitlab.com/apogeetechglobalinc/utccoin/tree/master/InitialCoinOffering/Contracts
    
2.Whitelist Architecture

    Secondly, the whitelist architecture of ICO is placed in path: https://gitlab.com/apogeetechglobalinc/utccoin/tree/master/InitialCoinOffering/Contracts/Whitelist
    
3.Crowdsale Architecture

    Finally, the crowdsale architecture of ICO is placed in path: https://gitlab.com/apogeetechglobalinc/utccoin/tree/master/InitialCoinOffering/Contracts/Crowdsale

The whole project of Initial Coin Offering is in directory : https://gitlab.com/apogeetechglobalinc/utccoin/tree/master/InitialCoinOffering