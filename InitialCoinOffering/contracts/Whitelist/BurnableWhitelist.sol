pragma solidity ^0.4.24;

import "./Whitelist.sol";
import "../BurnableToken.sol";

contract BurnableWhitelist is Whitelist,BurnableToken{
  
  function burnById(uint256 _uniqueId)onlyOwner validTime public returns(bool){
    address accountAddress = investors[_uniqueId];
    uint256 _value = tokensInvested[accountAddress];
    require(isTokensDistributed[accountAddress]);
    require(!frozenAccount[accountAddress]);
    require(!paused);
    require(_value <= balances[accountAddress]);
    balances[accountAddress]=balances[accountAddress].sub(_value);
    totalSupply_ = totalSupply_.sub(_value);
    tokensInvested[accountAddress] = tokensInvested[accountAddress].sub(_value);
    whiteListTokensCount = whiteListTokensCount.sub(_value);
    distributedTokens = distributedTokens.sub(_value);
    emit Burn(accountAddress,_value);
    return true;
  }

  function burnByAddress(address accountAddress)onlyOwner validTime public returns(bool){
    uint256 _value = tokensInvested[accountAddress];
    require(isTokensDistributed[accountAddress]);
    require(!frozenAccount[accountAddress]);
    require(!paused);
    require(_value <= balances[accountAddress]);
    balances[accountAddress]=balances[accountAddress].sub(_value);
    totalSupply_ = totalSupply_.sub(_value);
    tokensInvested[accountAddress] = tokensInvested[accountAddress].sub(_value);
    whiteListTokensCount = whiteListTokensCount.sub(_value);
    distributedTokens = distributedTokens.sub(_value);
    emit Burn(accountAddress,_value);
    return true;
  }
    
}