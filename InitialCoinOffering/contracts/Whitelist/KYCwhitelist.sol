pragma solidity ^0.4.24;

import "../BytesExtraction.sol";
import "./Whitelist.sol";

contract KYCwhitelist is Whitelist{
    
    using BytesExtraction for bytes;
    uint256 whitelistInvestorsViaKYC;
    
    struct investorInfo{
        uint128 whitelistInvestorUID;
        uint256 whitelistInvestorUniqueId;
        uint256 whitelistInvestorTokensAmount;  
    }
    
    mapping(address => investorInfo) whitelistInvestor;
    address[] whitelistInvestors;
    
    event OrderKYCInvestment(bytes data, uint256 _uniqueId, uint256 _tokensInvested);
    
    function orderKYCInvestment(bytes data, uint256 _uniqueId, uint256 _tokensInvested) public onlyOwner returns(bool){
        bool success;
        address _whitelistInvestorAddr = data.extractAddress(0);
        uint128 _whitelistInvestorUID = uint128(data.extract16bytes(20));
        success = addInvestor(_uniqueId, _whitelistInvestorAddr, _tokensInvested);
        require(success==true);
        whitelistInvestors.push(_whitelistInvestorAddr);
        whitelistInvestorsViaKYC = whitelistInvestorsViaKYC.add(1);
        whitelistInvestor[_whitelistInvestorAddr].whitelistInvestorUID = _whitelistInvestorUID;
        whitelistInvestor[_whitelistInvestorAddr].whitelistInvestorUniqueId = _uniqueId;
        whitelistInvestor[_whitelistInvestorAddr].whitelistInvestorTokensAmount = _tokensInvested;
        emit OrderKYCInvestment(data, _uniqueId, _tokensInvested);
        return success;
    }
    
    function getKYCInvestorDetails(address _kycInvestor)public view returns(uint128, uint256, uint256){
        return (whitelistInvestor[_kycInvestor].whitelistInvestorUID, whitelistInvestor[_kycInvestor].whitelistInvestorUniqueId, whitelistInvestor[_kycInvestor].whitelistInvestorTokensAmount);
    }
    
}