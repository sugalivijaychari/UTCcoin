pragma solidity ^0.4.24;

import "./Whitelist.sol";

contract WhitelistDistribution is Whitelist{
    
    event DistributeTokensById(uint256);
    event DistributeTokensByAddress(address);
    
    function distributeTokensById(uint256 _uniqueId)onlyOwner public returns(bool){
        address accountAddress = investors[_uniqueId];
        require(activeWhitelist[investors[_uniqueId]]);
        require(!paused);
        require(!isTokensDistributed[accountAddress]);
        require(!frozenAccount[accountAddress]);
        uint256 amount = tokensInvested[investors[_uniqueId]];
        transfer(investors[_uniqueId], amount);
        distributedTokens = distributedTokens.add(amount);
        emit DistributeTokensById(_uniqueId);
        return true;
    }
    
    function distributeTokensByAddress(address _investorAddr)onlyOwner public returns(bool){
        address accountAddress = _investorAddr;
        require(!paused);
        require(!isTokensDistributed[accountAddress]);
        require(!frozenAccount[accountAddress]);
        require(activeWhitelist[_investorAddr]);
        uint256 amount = tokensInvested[_investorAddr];
        transfer(_investorAddr, amount);
        distributedTokens = distributedTokens.add(amount);
        emit DistributeTokensByAddress(_investorAddr);
        return true;
    }
    
}