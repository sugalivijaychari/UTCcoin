pragma solidity ^0.4.24;

import "./Whitelist.sol";

contract RemovedWhitelist is Whitelist{
    
    // once a investor invests, funds are irreversible - so we can enable or disable the account
    
    function disableInvestor(address _investorAddr)public onlyOwner{
        activeWhitelist[_investorAddr]=false;
        freezeAccount(_investorAddr,true);
    }
    
    function enableInvestor(address _investorAddr)public onlyOwner{
        activeWhitelist[_investorAddr]=true;
        freezeAccount(_investorAddr,false);
    }
    
}