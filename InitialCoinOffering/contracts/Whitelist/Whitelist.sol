pragma solidity ^0.4.24;

import "../MintableToken.sol";

contract Whitelist is MintableToken{
    
    uint256 public startsAt;
    uint256 public endsAt;// This gonna be the starting time of crowdsale
    
    uint256[] public uniqueIds;
    uint256 public whiteListCount;
    uint256 public whiteListTokensCount;
    uint256 public distributedTokens;
    bool public _isEndTimeChanged;
    
    
    uint256 public limitOfTokensPerInvestor;
    uint256 public limitOfInvestors;
    uint256 public limitOfTotalTokens;
    bool public limitEnable;
    
    mapping(address => bool) public activeWhitelist;
    mapping(uint256 => address) public investors;
    mapping(address => uint256) public tokensInvested;
    mapping(address=>bool)public isTokensDistributed;
    
    function setTimings(uint256 _startsAt,uint256 _endsAt)onlyOwner public returns(bool){
        require(_startsAt<_endsAt);
        startsAt = _startsAt;
        endsAt = _endsAt;
        return true;
    }
    
    function enableLimits(bool _limitEnable)public onlyOwner{
        
        limitEnable = _limitEnable;
    }
    
    function setLimits(uint256 _limitOfTokensPerInvestor,uint256 _limitOfInvestors, uint256 _limitOfTotalTokens)public onlyOwner returns(bool){
        require((_limitOfTokensPerInvestor>0)&&(_limitOfInvestors>0)&&(_limitOfTotalTokens>0));
        limitOfTokensPerInvestor = _limitOfTokensPerInvestor;
        limitOfInvestors = _limitOfInvestors;
        limitOfTotalTokens = _limitOfTotalTokens;
        return true;
    }
    
    modifier validAccount{
        require(whiteListCount<limitOfInvestors && whiteListTokensCount<limitOfTotalTokens);
        _;
    }
    
    modifier validTime{
        require(now>=startsAt && now<endsAt);
        _;
    }
    
    event AddInvestor(uint256 _uniqueId, address investorAddr, uint256 _tokensInvested);
    
    function addInvestor(uint256 _uniqueId, address investorAddr, uint256 _tokensInvested) onlyOwner validTime validAccount public returns(bool){
        require(msg.sender == owner);
        require((_tokensInvested>0)&&(_tokensInvested<=limitOfTokensPerInvestor));
        require(!paused);
        whiteListTokensCount = whiteListTokensCount.add(_tokensInvested);
        uniqueIds.push(_uniqueId);
        investors[_uniqueId] = investorAddr; 
        tokensInvested[investorAddr] = _tokensInvested;
        mint(owner, _tokensInvested);
        whiteListCount +=1;
        activeWhitelist[investorAddr]=true;
        isTokensDistributed[investorAddr]=true;
        emit AddInvestor(_uniqueId, investorAddr, _tokensInvested);
        return true;
    }
    
    function extendEndTime(uint256 _endsAt)onlyOwner public returns(bool){
        require(_endsAt>startsAt);
        endsAt = _endsAt;
        _isEndTimeChanged = true;
        return true;
    }
    
    function reduceEndTime(uint256 _endsAt) onlyOwner public returns(bool){
        require(_endsAt>startsAt);
        endsAt = _endsAt;
        _isEndTimeChanged = true;
        return true;
    }
    
}