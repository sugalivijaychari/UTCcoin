pragma solidity ^0.4.24;

import "./Whitelist.sol";

contract WhitelistInfo is Whitelist{
    
    event DisplayInvestorTokens(address _investorAddr);
    
    function displayStartTime()public view returns(uint256){
        return startsAt;
    }
    
    function displayEndTime()public view returns(uint256){
        return endsAt;
    }
    
    function isThisValidTime() public view returns(bool){
        return (now>=startsAt && now<endsAt);
    }
    
    function displayWhiteInvestment()public view returns(uint256){
        return whiteListTokensCount;
    }
    
    function displayInvestorTokens(address _investorAddr)public returns(uint256){
        require(isTokensDistributed[_investorAddr]);
        require(activeWhitelist[_investorAddr]);
        emit DisplayInvestorTokens(_investorAddr);
        return tokensInvested[_investorAddr];
    }
    
    function displayDistributedTokens()public view returns(uint256){
        return distributedTokens;
    }
    
    function displayDistributableTokens()public view returns(uint256){
        return whiteListTokensCount.sub(distributedTokens);
    }
    
    function isEndTimeExtended()public view returns(bool){
        return _isEndTimeChanged;
    }
    
}