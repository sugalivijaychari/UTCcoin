pragma solidity ^0.4.24;

import "./CrowdsaleTokens.sol";

contract CrowdsalesAction is CrowdsaleTokens{
    
    mapping(address => bool) requestedDestroy;
    mapping(address => bool) finalizeRequest;
    mapping(address => bool) acceptedDestroy;
    mapping(address => uint256) opinionsChanged;
    
    event RequestToDestroy(uint256 _uniqueId, address _buyerAddr, bool _opinion);
    event DestroyCrowdsaler(uint256 _uniqueId, address _buyerAddr, uint256 _finalAmount);
    
    /**
     * The functions mentioned in this contract are 
     * very dangerous functions which
     * destroys his/her investment in crowdsale and he no longer
     * can make use of this ICO
     * Eventhough this gonna be a critical decision, but we can't judge future
     * as well inverstor have his own liberty to be a part of this
     * system or not.
     * In that aspect, this contract is developed!!
     */
    
    function requestToDestroyById(uint256 _uniqueId, bool _opinion) public _safeCrowdsale returns(bool){
        address _buyerAddr = crowdsaleAccounts[_uniqueId];
        bool success = requestToDestroy(_uniqueId, _buyerAddr,_opinion);
        return success;
    }
    
    function requestToDestroy(uint256 _uniqueId, address _buyerAddr, bool _opinion) public _safeCrowdsale returns(bool){
        require(!activeWhitelist[_buyerAddr]);
        require(!frozenAccount[_buyerAddr]);
        require(!finalizeRequest[_buyerAddr]);
        requestedDestroy[_buyerAddr] = _opinion;
        opinionsChanged[_buyerAddr] = opinionsChanged[_buyerAddr].add(1);
        emit RequestToDestroy(_uniqueId, _buyerAddr, _opinion);
        return true;
    }
    
    function acceptToDestroy(uint256 _uniqueId, address _buyerAddr, bool _opinion) public onlyOwner _safeCrowdsale returns(bool){
        require(!activeWhitelist[_buyerAddr]);
        require(!frozenAccount[_buyerAddr]);
        require(requestedDestroy[_buyerAddr]);
        acceptedDestroy[_buyerAddr] = _opinion;
        if(_opinion == true){
            finalizeRequest[_buyerAddr] = true;
            freezeAccount(_buyerAddr, _opinion);
            isATokenHolder[_buyerAddr] = false;
            isACrowdsaleAccountant[_uniqueId] = false;
            uint256 _finalAmount = addedTokens[_buyerAddr];
            uint256 _weiAmount = addedWeiAmount[_buyerAddr];
            addedTokens[_buyerAddr] = addedTokens[_buyerAddr].sub(_finalAmount);
            addedWeiAmount[_buyerAddr] = addedWeiAmount[_buyerAddr].sub(_weiAmount);
            orderedTokens = orderedTokens.sub(_finalAmount);
            orderedWeiAmount = orderedWeiAmount.sub(_weiAmount);
            totalSupply_ = totalSupply_.sub(_finalAmount);
        }
        emit DestroyCrowdsaler(_uniqueId, _buyerAddr, _finalAmount);
        return _opinion;
    }
    
}




