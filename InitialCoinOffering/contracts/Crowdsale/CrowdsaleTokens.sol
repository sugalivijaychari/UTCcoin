pragma solidity ^0.4.24;

import "./SafeCrowdsale.sol";

contract CrowdsaleTokens is SafeCrowdsale{
    
    event CalculateTokens(uint256 _weiAmount);
    event BuyCrowdsale(uint256 _uniqueId, address _buyerAddr, uint256 _weiAmount, uint256 _sellingPricePerToken, uint256 _finalAmount);
    
    function calculateTokens(uint256 _weiAmount, uint256 _sellingPricePerToken) public returns(uint noOfTokens){
        require((_weiAmount>0)&&(_sellingPricePerToken>0));
        uint amount = _weiAmount;
        amount = amount.div(_sellingPricePerToken);
        emit CalculateTokens(_weiAmount);
        return amount;
    }
    
    function buyCrowdsaleLocalAccount(uint256 _uniqueId, uint256 _sellingPricePerToken) _safeCrowdsale public payable returns(bool){
        uint256 _weiAmount = msg.value;
        address _buyerAddr = msg.sender;
        bool success = buyCrowdsale(_uniqueId, _weiAmount, _buyerAddr, _sellingPricePerToken);
        return success;
    }
    
    function buyCrowdsaleToOtherAccount(uint256 _uniqueId, address _buyerAddr, uint256 _sellingPricePerToken) _safeCrowdsale public payable returns(bool){
        uint256 _weiAmount = msg.value;
        bool success = buyCrowdsale(_uniqueId, _weiAmount, _buyerAddr, _sellingPricePerToken);
        return success;
    }
    
    function buyCrowdsale(uint256 _uniqueId, uint256 _weiAmount, address _buyerAddr, uint256 _sellingPricePerToken) _safeCrowdsale internal returns(bool){
        require(!activeWhitelist[_buyerAddr]);
        require(!frozenAccount[_buyerAddr]);
        require(_weiAmount> 0);
        uint256 _finalAmount = calculateTokens(_weiAmount, _sellingPricePerToken);
        require((orderedTokens.add(_finalAmount)) <= targetAmountOfTokens);
        require(addedTokens[_buyerAddr].add(_finalAmount)>=minAmountOfTokens);
        if(!isATokenHolder[_buyerAddr]){
            crowdsalersCount = crowdsalersCount.add(1);
        }
        crowdsaleAccounts[_uniqueId] = _buyerAddr;
        addedTokens[_buyerAddr] = addedTokens[_buyerAddr].add(_finalAmount);
        addedWeiAmount[_buyerAddr] = addedWeiAmount[_buyerAddr].add(_weiAmount);
        orderedTokens = orderedTokens.add(_finalAmount);
        orderedWeiAmount = orderedWeiAmount.add(_weiAmount);
        isACrowdsaleAccountant[_uniqueId] = true;
        isATokenHolder[_buyerAddr] = true;
        totalSupply_ = totalSupply_.add(_finalAmount);
        assert(!owner.send(_weiAmount));
        emit BuyCrowdsale(_uniqueId, _buyerAddr, _weiAmount, _sellingPricePerToken, _finalAmount);
        return true;
    }
    
}