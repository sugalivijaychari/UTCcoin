pragma solidity ^0.4.24;

import "./CrowdsaleCredentials.sol";

contract SafeCrowdsale is CrowdsaleCredentials{
    
    bool crowdsaleStatus;
    
    function setCrowdsaleStatus(bool _crowdsaleStatus) onlyOwner public{
        crowdsaleStatus = _crowdsaleStatus;
    } 
    
    modifier _safeCrowdsale{
        require(crowdsaleStatus && (now>=startTime && now<endTime) && (!paused));
        _;
    }
    
    modifier _unSafeCrowdsale{
        require(!crowdsaleStatus && (now>=startTime && now<endTime) && (paused));
        _;
    }
    
}