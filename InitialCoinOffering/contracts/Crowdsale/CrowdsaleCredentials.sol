pragma solidity ^0.4.24;

import "../Whitelist/Whitelist.sol";

contract CrowdsaleCredentials is Whitelist{
    
    uint256 public startTime;
    uint256 public endTime;
    uint256 public targetAmountOfTokens;
    uint256 public minAmountOfTokens;
    uint256 public suppliedTokens;//*
    uint256 public orderedTokens;
    uint256 public orderedWeiAmount;
    uint256 public crowdsalersCount;
    uint256 public suppliedCrowdsalersCount;//*
    
    mapping(uint256 => address) public crowdsaleAccounts;
    mapping(address => uint256) public addedTokens;
    mapping(address => uint256) public addedWeiAmount;
    mapping(uint256 => bool) public isACrowdsaleAccountant;
    mapping(address => bool) public isATokenHolder;
    mapping(address => bool) public isTokensSupplied;//*
    
    function setStartTime() public onlyOwner returns(bool){
        require(startTime>=endsAt);
        startTime = endsAt;
        return true;
    }
    
    function setEndTime(uint256 _endtime) public onlyOwner returns(bool){
        require(_endtime >startTime);
        endTime = _endtime;
        return true;
    }
    
    function resetStartTime(uint256 _startTime) public onlyOwner returns(bool){
        require(_startTime <endTime);
        startTime = _startTime;
        return true;
    }
    
    function resetEndTime(uint256 _endtime) public onlyOwner returns(bool){
        require(_endtime>startTime);
        endTime = _endtime;
        return true;
    }
    
    function setTargetAmountOfTokens(uint256 _targetAmount) public onlyOwner returns(bool){
        require(_targetAmount>0);
        targetAmountOfTokens = _targetAmount;
        return true;
    }
    
    function setMinimumAmountOfTokens(uint256 _minAmount) public onlyOwner returns(bool){
        require(_minAmount<=targetAmountOfTokens);
        minAmountOfTokens = _minAmount;
        return true;
    }
    
}