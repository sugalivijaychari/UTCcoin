pragma solidity ^0.4.24;

import "./CrowdsalesAction.sol";
import "./DisplayableCrowdsale.sol";

contract SuppliableCrowdsale is CrowdsalesAction,DisplayableCrowdsale{
    
    uint256 public _validSupplyTime;
    
    event SupplyTokens(uint256 _uniqueId, address _crowdsalerAddr, uint256 value);
    
    function validSupplyTime() public view returns(bool){
        require(!paused);
        require((now>startTime)&&(now>endTime)&&(!crowdsaleStatus)&&(!isCrowdsaleActive()));
        return true;
    }
    
    modifier canSupply{
        require(validSupplyTime());
        _;
    }
    
    function supplyTokens(uint256 _uniqueId, address _crowdsalerAddr) public onlyOwner canSupply returns(bool){
    
        require(!activeWhitelist[_crowdsalerAddr]);
        require(!frozenAccount[_crowdsalerAddr]);
        require(!finalizeRequest[_crowdsalerAddr]);
        require(!acceptedDestroy[_crowdsalerAddr]);
        require((addedTokens[_crowdsalerAddr]>0)&&(addedWeiAmount[_crowdsalerAddr]>0));
        require(!isACrowdsaleAccountant[_uniqueId]);
        require(!isATokenHolder[_crowdsalerAddr]);
        require(!isTokensSupplied[_crowdsalerAddr]);
        uint256 amount = addedTokens[_crowdsalerAddr];
        suppliedTokens = suppliedTokens.add(amount);
        suppliedCrowdsalersCount = suppliedCrowdsalersCount.add(1);
        isTokensSupplied[_crowdsalerAddr] = true;
        transfer(_crowdsalerAddr, amount);
        emit SupplyTokens(_uniqueId, _crowdsalerAddr, amount);
        return true;
        
    }
    
}