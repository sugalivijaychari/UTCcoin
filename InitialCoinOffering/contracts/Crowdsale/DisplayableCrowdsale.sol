pragma solidity ^0.4.24;

import "./CrowdsalesAction.sol";

contract DisplayableCrowdsale is CrowdsalesAction{
    
    bool public _isCrowdsalePaused = (crowdsaleStatus && paused);
    
    
    function isCrowdsalePaused() public view returns(bool){
        return _isCrowdsalePaused;
    }
    
    function viewStartTime()public view returns(uint256){
        return startTime;
    }
    
    function viewEndTime()public view returns(uint256){
        return endTime;
    }
    
    function isCrowdsaleActive()public view returns(bool){
        return (_isCrowdsalePaused && (now>=startTime && now<endTime) );
    }
    
    function viewTargetTokens()public view returns(uint256){
        return targetAmountOfTokens;
    }
    
    function viewMinimumTokens()public view returns(uint256){
        return minAmountOfTokens;
    }
    
    function viewOrderedTokens()public view returns(uint256){
        return orderedTokens;
    }
    
    function viewOrderedWeiAmount()public view returns(uint256){
        return orderedWeiAmount;
    }
    
    function viewCrowdsalersCount()public view returns(uint256){
        return crowdsalersCount;
    }
    
    function viewAddressOfCrowdsaleId(uint256 _uniqueId)public view returns(address){
        return crowdsaleAccounts[_uniqueId];
    }
    
    function viewIncentiveOfCrowdsaler(address _crowdsalerAddr) public view returns(uint256){
        return addedTokens[_crowdsalerAddr];
    }
    
    function weiAddedByCrowdsaler(address _crowdsalerAddr) public view returns(uint256){
        return addedWeiAmount[_crowdsalerAddr];
    }
    
    function viewStatusOfCrowdsaler(uint256 _uniqueId, address _crowdsalerAddr) public view returns(bool){
        return ((isACrowdsaleAccountant[_uniqueId])&&(isATokenHolder[_crowdsalerAddr]));
    }
    
    function isCrowdsalerLeft(address _crowdsalerAddr) public view returns(bool){
        return (finalizeRequest[_crowdsalerAddr] && acceptedDestroy[_crowdsalerAddr]);
    }
    
    function isCrowdsalerRequestedToLeft(address _crowdsalerAddr)public view returns(bool){
        return (requestedDestroy[_crowdsalerAddr]);
    }
    
    function crowdsalerOpinionsCount(address _crowdsalerAddr)public view returns(uint256){
        return opinionsChanged[_crowdsalerAddr];
    }
    
}







