pragma solidity ^0.4.24;

import "./MintableToken.sol";

contract LimitMintToken is MintableToken {

  uint256 public limit;

  constructor(uint256 _limit) public {
    require(_limit > 0);
    limit = _limit;
  }

  function mint(address _to, uint256 _amount) onlyOwner canMint public returns (bool) {
    require(totalSupply_.add(_amount) <= limit);

    return super.mint(_to, _amount);
  }

}
