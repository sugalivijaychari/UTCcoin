pragma solidity ^0.4.24;


import "./ERC20Basic.sol";
import "./SafeMath.sol";
import "./UTCcoin.sol";

contract BasicToken is ERC20Basic,UTCcoin {
  using SafeMath for uint256;

  mapping(address => uint256) balances;
  
  

  uint256 totalSupply_;

  constructor()public{
    totalSupply_ = initialSupply.mul(10** uint256(decimals));
  }

  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }

  function transfer(address _to, uint256 _value) public returns (bool) {
    require(_to != address(0));
    require(_value <= balances[msg.sender]);
    balances[msg.sender] = balances[msg.sender].sub(_value);
    balances[_to] = balances[_to].add(_value);
    emit Transfer(msg.sender, _to, _value);
    return true;
  }

  function balanceOf(address _owner) public view returns (uint256 balance) {
    return balances[_owner];
  }

}
