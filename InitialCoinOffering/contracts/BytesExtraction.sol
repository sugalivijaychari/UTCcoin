pragma solidity ^0.4.24;

library BytesExtraction{
    
    function extract32bytes(bytes data, uint256 index) public pure returns(bytes32){
        bytes32 result;
        for(uint256 i=0; i<32; i++){
            result |= bytes32(data[index+i] & 0xFF) >> (i*8);
        }
        return result;    
    }
    
    function extractAddress(bytes data, uint256 index) public pure returns (address){
        bytes32 result;
        for(uint256 i=0; i< 20; i++){
            result |= bytes32(data[index +i] & 0xFF) >> ((i+12) * 8);
        }
        return address(uint256(result));
    }
    
    function extract16bytes(bytes data, uint256 index) public pure returns(bytes16){
        bytes16 result;
        for(uint256 i=0; i< 16; i++){
            result |= bytes16(data[index +i] & 0xFF) >> (i * 8);
        }
        return result;
    }
       
}