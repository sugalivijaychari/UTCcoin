pragma solidity ^0.4.24;

contract UTCcoin{

  string public name;
  uint256 public initialSupply;
  string public symbol;
  uint8 public decimals;

  constructor(string _name, uint256 _initialSupply,string _symbol, uint8 _decimals) public {
    name = _name;
    initialSupply = _initialSupply;
    symbol = _symbol;
    decimals = _decimals;
  }
}
