pragma solidity ^0.4.24;

import "./BasicToken.sol";
import "./Ownable.sol";

contract BurnableToken is BasicToken, Ownable {
    
  event Burn(address indexed burner, uint256 value);

  function burn(uint256 _value) public {
    require(_value <= balances[msg.sender]);
    address burner = msg.sender;
    balances[burner] = balances[burner].sub(_value);
    totalSupply_= totalSupply_.sub(_value);
    emit Burn(burner, _value);
    emit Transfer(burner, address(0), _value);
  }

  function burnFrom(address _from, uint256 _value) public onlyOwner returns(bool success){
    require(balances[_from] >= _value);
    address burner = _from;
    balances[burner] = balances[burner].sub(_value);
    totalSupply_= totalSupply_.sub(_value);
    emit Burn(burner, _value);
    emit Transfer(burner, address(0), _value);
    return true;
  }

}
