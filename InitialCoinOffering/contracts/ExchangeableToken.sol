pragma solidity ^0.4.24;

import "./FreezableToken.sol";

contract ExchangeableToken is FreezableToken{
    
    uint256 public sellingPrice;
    uint256 public buyingPrice;
    
    function setExchangePrices(uint256 _sellingPrice, uint256 _buyingPrice)public{
        sellingPrice = _sellingPrice;
        buyingPrice = _buyingPrice;
    }
    
    function buyTokens()public payable returns(uint256 amount){
        amount = msg.value/buyingPrice;
        transferFrom(this,msg.sender,amount);
    }
    
    function sellTokens(uint256 amount)public{
        address myAddress = this;
        require(myAddress.balance >= amount*sellingPrice);
        transferFrom(msg.sender,this,amount);
        msg.sender.transfer(amount*sellingPrice);
    } 
    
}